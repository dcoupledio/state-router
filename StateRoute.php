<?php namespace Decoupled\Core\State;

use Decoupled\Core\State\StateParserInterface;
use Decoupled\Core\Action\ActionFactoryInterface;

class StateRoute implements StateRouteInterface{
    
    const STATE_TYPE_DEFAULT = 'state.default';

    const STATE_TYPE_ANY = 'state.any';

    const STATE_TYPE_EXCLUDE = 'state.exclude';

    const STATE_TYPE_EXCLUDE_ANY = 'state.exclude.any';

    /**
     * when all the states in this array are
     * found within application state array
     * it will match this route
     *
     * @var        array
     */

    protected $states = [];

    /**
     * callback fired when route is matched
     *
     * @var        Decoupled\Core\Action\ActionInterface
     */

    protected $action  = null;

    /**
     * id of the current route
     *
     * @var        string
     */

    protected $name = null;

    /**
     * converts raw action into Decoupled\Core\Action\ActionInterface
     * 
     * @var        Decoupled\Core\Action\ActionFactoryInterface
     */

    protected $actionFactory;

    /**
     * Converts raw state list into array
     * 
     * @var       Decoupled\Core\State\StateParserInterface
     */

    protected $parser;

    /**
     * alias for Decoupled\Core\State\StateRoute::setAction
     *
     * @param      mixed  $callback
     *
     * @return     Decoupled\Core\State\StateRoute (self)
     */    

    public function uses( $callback )
    {
        $this->setAction( $callback );

        return $this;
    }


    /**
     * adds exclude type states to route
     *
     * @param      mixed  $states  The states
     *
     * @return     Decoupled\Core\State\StateRoute (self)
     */    

    public function not( $states )
    {
        $states = $this->getStateParser()->parse( $states );

        return $this->addStates( $states, self::STATE_TYPE_EXCLUDE );
    }

    /**
     * adds exclude any type states
     *
     * @param      <type>  $states  The states
     *
     * @return     <type>  ( description_of_the_return_value )
     */

    public function notAny( $states )
    {
        $states = $this->getStateParser()->parse( $states );

        return $this->addStates( $states, self::STATE_TYPE_EXCLUDE_ANY );
    }   

    /**
     * alias for Decoupled\Core\State\StateRoute::addStates
     * that accepts non-array state list
     *
     * @param      mixed  $states  The states
     *
     * @return     Decoupled\Core\State\StateRoute (self)
     */

    public function when( $states )
    {
        $states = $this->getStateParser()->parse( $states );

        return $this->addStates( $states, self::STATE_TYPE_DEFAULT );
    }

    /**
     * alias for Decoupled\Core\State\StateRoute::addAnyStates
     * that accepts non-array state list
     *
     * @param      mixed  $states  The states
     *
     * @return     Decoupled\Core\State\StateRoute (self)
     */

    public function any( $states )
    {
        $states = $this->getStateParser()->parse( $states );

        return $this->addStates( $states, self::STATE_TYPE_ANY );
    }

    /**
     * Adds a states to route state array.
     *
     * @param      array   $states  The states
     *
     * @return     Decoupled\Core\State\StateRoute ( self )
     */

    public function addStates( array $states, $type = null )
    {
        $key = ((string) $type) ?: self::STATE_TYPE_DEFAULT;

        $this->states[$key] = array_merge(
            @$this->states[$key] ?: [],
            $states
        );

        return $this;
    }

    /**
     * Sets the route states.
     *
     * @param      array   $states  The states
     *
     * @return     Decoupled\Core\State\StateRoute ( self )
     */

    public function setStates( array $states, $type = null )
    {
        $this->states[ ( $type ?: self::STATE_TYPE_DEFAULT ) ] = $states;    
        
        return $this;
    }

    /**
     * Gets the route states.
     *
     * @return     arrray  The states.
     */

    public function getStates( $type = null )
    {
        return @$this->states[ ( $type ?: self::STATE_TYPE_DEFAULT ) ];
    }

    /**
     * Removes a states from array 
     *
     * @param      array   $names  The names of states to remove
     * @param      string  $type   The type of state
     *
     * @return     Decoupled\Core\State\StateRoute ( self )
     */

    public function removeStates( array $names, $type = null )
    {
        $key = ((string) $type) ?: self::STATE_TYPE_DEFAULT;

        foreach( $names as $name )
        {
            if( false !== ($found = array_search($name, $this->states[$key])) )
            {
                unset( $this->states[$key][$found] );
            }
        }

        return $this;
    }

    /**
     * Sets the action callback associated with this route.
     *
     * @param      mixed  $callback  The callback
     *
     * @return     Decoupled\Core\State\StateRoute ( self )
     */

    public function setAction( $callback )
    {
        $this->action = $this
            ->getActionFactory()
            ->make( $callback );

        return $this;
    }

    /**
     * Gets the action.
     *
     * @return     Decoupled\Core\Action\ActionInterface The action.
     */

    public function getAction()
    {
        return $this->action;
    }

    /**
     * Sets the action factory.
     *
     * @param      \Decoupled\Core\Action\ActionFactoryInterface  $factory  The factory object
     *
     * @return     Decoupled\Core\State\StateRoute ( self )
     */

    public function setActionFactory( ActionFactoryInterface $factory )
    {
        $this->actionFactory = $factory;

        return $this;
    }

    /**
     * Gets the action factory.
     *
     * @return     Decoupled\Core\Action\ActionFactoryInterface  The action factory.
     */

    public function getActionFactory()
    {
        return $this->actionFactory;
    }

    /**
     * Sets the state parser.
     *
     * @param      \Decoupled\Core\State\StateParserInterface  $parser  The parser
     *
     * @return     Decoupled\Core\State\StateRoute ( self )
     */

    public function setStateParser( StateParserInterface $parser )
    {
        $this->parser = $parser;

        return $this;
    }

    /**
     * Gets the state parser.
     *
     * @return     \Decoupled\Core\State\StateParserInterface  The state parser.
     */

    public function getStateParser()
    {
        return $this->parser;
    }
}