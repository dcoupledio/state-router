<?php namespace Decoupled\Core\State;

class StateRouteCollection implements StateRouteCollectionInterface{

    protected $routes = [];

    /**
     * Push route to the stack
     *
     * @param      string               $name   The name
     * @param      StateRouteInterface  $route  The route
     * 
     * @return     Decoupled\Core\State\StateRouteCollection ( self )
     */

    public function add( $name, StateRouteInterface $route )
    {
        $this->routes[$name] = $route;

        return $this;
    }

    /**
     * gets create route from collection
     *
     * @param      string  $name   The route name
     *
     * @return     StateRouteInterface|null  The route or null if doesn't exist
     */    

    public function get( $name )
    {
        return @$this->routes[$name];
    }

    /**
     * remove route from the stack by it's name
     *
     * @param      string  $name   The route name
     * 
     * @return     Decoupled\Core\State\StateRouteCollection ( self )
     */

    public function remove( $name )
    {
        unset( $this->routes[$name] );

        return $this;
    }

    /**
     * @return     array of $objects implementing StateRouteInterface 
     */

    public function all()
    {
        return $this->routes;
    }

}