<?php namespace Decoupled\Core\State;

class StateParser implements StateParserInterface{

    /**
     * Parses given states into array of states
     *
     * @param      mixed  $states  The states
     * 
     * @return     array states
     */

    public function parse( $states )
    {
        if( is_array($states) ) return $states;

        return array_filter( explode(" ", $states) );
    }
}