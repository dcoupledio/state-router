<?php namespace Decoupled\Core\State;

interface StateRouteResolverInterface{

    /**
     * matches given routes to state array and returns their actions
     *
     * @param      array                          $state   The state
     * @param      StateRouteCollectionInterface  $routes  The routes
     */

    public static function resolveActions( array $state, StateRouteCollectionInterface $routes );

    /**
     * matches single route to state array and returns route action
     *
     * @param      array                $state  The state
     * @param      StateRouteInterface  $route  The route
     */

    public static function resolveAction( array $state, StateRouteInterface $route );
}