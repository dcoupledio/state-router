<?php namespace Decoupled\Core\State;

use Decoupled\Core\State\StateRoute;
use Decoupled\Core\Action\ActionFactoryInterface;

class StateRouteFactory implements StateRouteFactoryInterface{

    /**
     * Creates new StateRoute Instance
     *
     * @param      mixed     $states  The states
     * @param      <type>      $name    The name
     *
     * @return    Decoupled\Core\State\StateRoute  new state route instance
     */

    public function make( $states = [], $name = null )
    {
        $route = new StateRoute();

        $route
        ->setActionFactory(
            $this->getActionFactory()
        )
        ->setStateParser(
            $this->getParser()
        );

        if( $states ) 
            $route->when( $states );

        if( $name )
            $route->setName( $name );

        return $route;
    }

    /**
     * Sets the action factory.
     *
     * @param      \Decoupled\Core\Action\ActionFactoryInterface  $factory  The factory
     *
     * @return     Decoupled\Core\State\StateRoute  ( self )
     */

    public function setActionFactory( ActionFactoryInterface $factory )
    {
        $this->actionFactory = $factory;

        return $this;
    }

    /**
     * Gets the action factory.
     *
     * @return     \Decoupled\Core\Action\ActionFactoryInterface  The action factory.
     */

    public function getActionFactory()
    {
        return $this->actionFactory;
    }

    /**
     * Sets the parser.
     *
     * @param      Decoupled\Core\State\StateParserInterface  $parser  The parser
     *
     * @return     Decoupled\Core\State\StateRoute  ( self )
     */

    public function setParser( StateParserInterface $parser )
    {
        $this->parser = $parser;

        return $this;
    }

    /**
     * Gets the parser.
     *
     * @return     Decoupled\Core\State\StateParserInterface  The parser.
     */

    public function getParser()
    {
        return $this->parser;
    }
}