<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;

use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\State\StateRoute;
use Decoupled\Core\State\StateRouteFactory;
use Decoupled\Core\State\StateParser;
use Decoupled\Core\State\StateRouteResolver;
use Decoupled\Core\State\StateRouter;
use Decoupled\Core\State\StateRouteCollection;

class RouteTest extends TestCase{

    protected $actionFactory;

    protected $router;

    protected $routeFactory;

    public function getActionFactory()
    {
        if( $this->actionFactory ) return $this->actionFactory;

        $this->actionFactory = new ActionFactory();

        $this->actionFactory->setInvoker( new ActionInvoker() );

        return $this->actionFactory;
    }

    public function getRouteFactory()
    {
        if( $this->routeFactory ) return $this->routeFactory;

        $this->routeFactory = $factory = new StateRouteFactory();

        $factory->setActionFactory( $this->getActionFactory() );

        $factory->setParser( new StateParser() );

        return $factory;
    }

    public function getRouter()
    {
        if( $this->router ) return $this->router;

        $this->router = new StateRouter();

        $this->router->setCollection( new StateRouteCollection() );

        $this->router->setRouteFactory( $this->getRouteFactory() );

        $this->router->setResolver( new StateRouteResolver() );

        return $this->router;
    }

    public function testCanMakeRoute()
    {
        $factory = $this->getRouteFactory();

        $route = $factory->make();

        $this->assertInstanceOf( StateRoute::class, $route );

        return $route;
    }

    /**
     * @depends testCanMakeRoute
     */

    public function testCanAssignDefaultStates( $route )
    {
        $states = $route->when(['state1','state2'])->getStates();

        $this->assertContains( 'state1', $states );

        $this->assertEquals( count($states), 2 );

        return $route;
    }

    /**
     * @depends testCanAssignDefaultStates
     */

    public function testCanAssignStateTypes( $route )
    {
        $states = $route
            ->any(['any1','any2'])
            ->getStates( StateRoute::STATE_TYPE_ANY );

        $this->assertContains( 'any2', $states );

        $excluded = $route
            ->not(['not1','not2'])
            ->getStates( StateRoute::STATE_TYPE_EXCLUDE );

        $this->assertContains( 'not1', $excluded );

        $excludeAny = $route
            ->notAny(['not.any'])
            ->getStates( StateRoute::STATE_TYPE_EXCLUDE_ANY );

        $this->assertContains( 'not.any', $excludeAny );   

        return $route; 
    }

    /**
     * @depends testCanAssignDefaultStates
     */

    public function testCanAssignStateAsString( $route )
    {
        $states = $route->any(' stateAsString stateAsString2 ')
            ->getStates( StateRoute::STATE_TYPE_ANY );

        $this->assertContains( 'stateAsString2', $states );
    }

    public function testCanResolveRouteActions()
    {
        $state = $this->getRouter();

        $state('test.state')
            ->when('state1 state2')
            ->uses(function(){

                return 1;
        });

        $actions = $state->resolve(['state1']);

        $this->assertEmpty( $actions );

        $actions = $state->resolve(['state1', 'state2']);

        $this->assertEquals( count($actions), 1 );

        $this->assertEquals( $actions[0](), 1 );

        return $state;
    }

    /**
     * @depends testCanResolveRouteActions
     */

    public function testCanResolveProperActions( $state )
    {
        $state('test.any.state')
            ->any(' any1 any2 ')
            ->uses( function(){ return 1; } );

        $actions = $state->resolve(['any1']);

        $this->assertEquals( count($actions), 1 );

        $this->assertEquals( $actions[0](), 1 );

        return $state;
    }

    /**
     * @depends testCanResolveProperActions
     */

    public function testCanExcludeRoute( $state )
    {
        $state('test.any.state')
            ->notAny('excluded');

        $actions = $state->resolve(['any1', 'excluded']);

        $this->assertEmpty( $actions );

        $actions = $state->resolve(['any1', 'state1', 'state2', 'excluded']);

        $this->assertEquals( count($actions), 1 );

        $state('test.not.exlcuded')
            ->not('state1 state2')
            ->when('any1 any2')
            ->uses(function(){ return 1; });

        $actions = $state->resolve(['any1', 'any2', 'excluded', 'state1']);

        $this->assertEquals( count($actions), 1 );

        $this->assertEquals( $actions[0](), 1 );
    }

}