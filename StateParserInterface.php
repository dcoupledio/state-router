<?php namespace Decoupled\Core\State;

interface StateParserInterface{

    /**
     * Parses given states into array of states
     *
     * @param      mixed  $states  The states
     */

    public function parse( $states );
}