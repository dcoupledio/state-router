<?php namespace Decoupled\Core\State;

use Decoupled\Core\Bootstrap\BootableInterface;
use Decoupled\Core\Application\Application;
use Decoupled\Core\State\StateRouter;
use Decoupled\Core\State\StateRouteFactory;
use Decoupled\Core\State\StateRoute;

class StateRouteResolver implements StateRouteResolverInterface{

    /**
     * Accepts Route collection and runs each through ::resolveAction
     *
     * @param      array                          $state   The state
     * @param      StateRouteCollectionInterface  $routes  The routes
     *
     * @return     array                         An array of inokeable actions
     */

    public static function resolveActions( array $state, StateRouteCollectionInterface $routes )
    {
        $actions = [];

        foreach( $routes->all() as $route )
        {
            $actions[] = self::resolveAction( $state, $route );
        }

        return array_values(array_filter( $actions ));
    }

    /**
     * Resolves the Route Action, by assigned states
     *
     * @param      array                $state  The state
     * @param      StateRouteInterface  $route  The route
     *
     * @return     boolean|ActionInterface Action if route matches state, false otherwise
     */

    public static function resolveAction( array $state, StateRouteInterface $route )
    {
        //if ALL of the states inside the $excluded array are found return action

        $excluded = $route->getStates( StateRoute::STATE_TYPE_EXCLUDE ) ?: [];

        if( self::isState( $state, $excluded ) ) return false;

        //if any of the states in the excludeAny array are found, return false

        $excludeAny = $route->getStates( StateRoute::STATE_TYPE_EXCLUDE_ANY ) ?: [];

        if( self::isState( $state, $excludeAny, true ) )
        {
            return false;   
        } 

        //if any of the states in the "any" array are found, return action

        $any = $route->getStates( StateRoute::STATE_TYPE_ANY ) ?: [];

        if( self::isState( $state, $any, true ) ) return $route->getAction();

        //if all the states in the default array are found, return action

        $all = $route->getStates( StateRoute::STATE_TYPE_DEFAULT );

        if( self::isState( $state, $all ) ) return $route->getAction();

        //return false if not apply

        return false;
    }

    /**
     * Determines if assigned states match the state array.
     *
     * @param      array    $state           The state array
     * @param      array    $assignedStates  The assigned states
     * @param      boolean  $any             if true, returns true on single match
     *
     * @return     boolean  True if assigned state matches given state, False otherwise.
     */

    public static function isState( array $state, array $assignedStates, $any = false )
    {
        if( !$any && (count($state) < count($assignedStates)) || empty($assignedStates) ) 
        {
            return false;
        }

        foreach( $assignedStates as $assigned )
        {
            //if assigned state not found in state array
            //and "any" is false, return false

            if( !in_array( $assigned, $state ) && !$any )
            {
                return false;
            }

            //if any of assignedStates found inside state array
            //return true when any is set to true 

            if( in_array( $assigned, $state ) && $any )
            {
                return true;
            }
        }

        //if any is true, and we're here, that means that the state
        //was not found in assigned list. return false
        //other wise, all were found

        return $any ? false : true;
    }

}