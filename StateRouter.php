<?php namespace Decoupled\Core\State;

use Decoupled\Core\State\StateRouteFactoryInterface;
use Decoupled\Core\State\StateRouteCollectionInterface;
use Decoupled\Core\State\StateRouteInterface;
use Decoupled\Core\State\StateRouteResolverInterface;
use Decoupled\Core\Action\ActionFactoryInterface;

class StateRouter{

    /**
     * @var \Decoupled\Core\State\StateRouteCollectionInterface
     */

    protected $routes;

    /**
     * service responsible for creating StateRoute instances
     * 
     * @var \Decoupled\Core\State\StateRouteFactoryInterface
     */

    protected $factory;

    /**
     * service responsible for resolving route actions based on given states
     * 
     * @var \Decoupled\Core\State\StateRouteResolverInterface
     */

    protected $resolver;

    /**
     * alias for ::get method
     *
     * @param      string $name   The name
     *
     * @return     \Decoupled\Core\State\StateRouteInterface
     */

    public function __invoke( $name )
    {
        return $this->get( $name );
    }

    /**
     * Sets the route collection object
     *
     * @param      \Decoupled\Core\State\StateRouteCollectionInterface  $routes  The route collection
     *
     * @return     \Decoupled\Core\State\StateRouter (self)
     */

    public function setCollection( StateRouteCollectionInterface $routes )
    {
        $this->routes = $routes;

        return $this;
    }

    /**
     * Gets the route collection.
     *
     * @return     \Decoupled\Core\State\StateRouteCollectionInterface  The collection.
     */

    public function getCollection()
    {
        return $this->routes;
    }

    /**
     * Sets the State Route factory.
     *
     * @param      \Decoupled\Core\State\StateRouteFactoryInterface  $factory  The factory
     *
     * @return     \Decoupled\Core\State\StateRouter (self)
     */

    public function setRouteFactory( StateRouteFactoryInterface $factory )
    {
        $this->factory = $factory;

        return $this;
    }

    /**
     * Gets the route factory.
     *
     * @return     Decoupled\Core\State\StateRouteFactoryInterface  The route factory.
     */

    public function getRouteFactory()
    {
        return $this->factory;
    }

    /**
     * factory method to create Decoupled\Core\Routing\StateRoute instance
     *
     * @param      array   $states  states assigned to route
     *
     * @return     Decoupled\Core\Routing\StateRoute  ( new instance )
     */

    public function make( $states = [] )
    {
        return $this
            ->getRouteFactory()
            ->make( $states );
    }    

    /**
     * Sets the resolver. service responsible for resolving matching routes
     *
     * @param      Decoupled\Core\State\StateRouteResolverInterface  $resolver  The resolver
     *
     * @return     Decoupled\Core\State\StateRouter ( self )
     */

    public function setResolver( StateRouteResolverInterface $resolver )
    {
        $this->resolver = $resolver;

        return $this;
    }

    /**
     * Gets the resolver.
     *
     * @return     Decoupled\Core\State\StateRouteResolverInterface  The resolver.
     */

    public function getResolver()
    {
        return $this->resolver;
    }

    /**
     * Attempts to resolve actions assigned to given state array
     *
     * @param      array   $state  The state
     *
     * @return     array  ( actions assigned to state )
     */

    public function resolve( array $state )
    {
        return $this
            ->getResolver()
            ->resolveActions( $state, $this->getCollection() );
    }    

    /**
     * gets route from collection, or creates a new route and pushes
     * to collection
     *
     * @param      string  $name   The name
     *
     * @return     Decoupled\Core\Routing\StateRoute  ( instance from collection )
     */

    public function get( $name )
    {
        $name = trim($name);

        $routes = $this->getCollection();

        //return route if it exists
        if( $route = $routes->get($name) ) return $route;

        //otherwise create new, and append to collection
        $route = $this->make();

        $routes->add( $name, $route );

        return $route;
    }


} 