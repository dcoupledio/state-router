<?php namespace Decoupled\Core\State;

interface StateRouteCollectionInterface{

    /**
     * Push route to the stack
     *
     * @param      string               $name   The name
     * @param      StateRouteInterface  $route  The route
     */

    public function add( $name, StateRouteInterface $route );

    /**
     * gets create route from collection
     *
     * @param      string  $name   The route name
     *
     * @return     StateRouteInterface|null  The route or null if doesn't exist
     */

    public function get( $name );

    /**
     * remove route from the stack by it's name
     *
     * @param      string  $name   The route name
     */

    public function remove( $name );

    /**
     * @return     array of $objects implementing StateRouteInterface 
     */

    public function all();

}