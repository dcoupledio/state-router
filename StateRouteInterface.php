<?php namespace Decoupled\Core\State;

interface StateRouteInterface{

    public function when( $states );

    public function not( $states );

    public function any( $states );

    public function notAny( $states );

    public function uses( $callback );

    public function addStates( array $states, $type = null );

    public function removeStates( array $states, $type = null );

    public function setStates( array $states, $type = null );

    public function getStates( $type = null );

    public function setAction( $callback );

    public function getAction();
}