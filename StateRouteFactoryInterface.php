<?php namespace Decoupled\Core\State;

interface StateRouteFactoryInterface{

    public function make( $states = [], $name = null );
}