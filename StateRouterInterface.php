<?php namespace Decoupled\Core\State;

interface StateRouterInterface{

    public function __invoke( $routeName );

    public function getCollection();

    public function resolve( array $states );
}